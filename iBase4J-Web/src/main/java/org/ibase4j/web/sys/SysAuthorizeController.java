package org.ibase4j.web.sys;

import org.ibase4j.core.base.BaseController;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

/**
 * 权限管理
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:14:05
 */
@RestController
@Api(value = "权限管理", description = "权限管理")
public class SysAuthorizeController extends BaseController {

}
